import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as img;

import '../pages/utils.dart';

class FaceDetectController extends GetxController {
  RxInt currentStep = 1.obs;
  final randomNum = Random();
  RxInt start = 5.obs;
  RxBool imageSaved = false.obs;
  RxBool isCorrectCondition = false.obs;
  RxBool isCompletedAllStep = false.obs;
  RxList<File> imageList = List<File>.empty().obs;
  Rxn<Timer?> time = Rxn<Timer>();

  void _startTimer() {
    if (time.value != null) return;
    const oneSec = Duration(seconds: 1);
    time.value = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (start.value == 0) {
          timer.cancel();
          time.value = null;
        } else {
          start.value--;
        }
      },
    );
  }

  void resetTimer() {
    time.value?.cancel();
    time.value = null;
    start.value = 5;
  }

  void stepProcess({
    required InputImage inputImage,
    required Face face,
    Function? startProcess,
    Function? onNextStep,
  }) {
    if (currentStep.value == 1) {
      _detectCondition(
        inputImage: inputImage,
        stepCondition: _stepOne(face),
      );
    } else if (currentStep.value == 2) {
      _detectCondition(
        inputImage: inputImage,
        stepCondition: _stepTwo(face),
      );
    } else {
      _detectCondition(
        inputImage: inputImage,
        stepCondition: _stepThree(face),
        onSavedImage: () => isCompletedAllStep.value = true,
      );
    }
  }

  Future<void> _saveImage({required InputImage inputImage}) async {
    final Directory tempDir = await getTemporaryDirectory();
    final img.Image imgDecode = decodeYUV420SP(inputImage);

    final int num = randomNum.nextInt(10);
    final file = File('${tempDir.path}/${num}front.png');
    if (file.existsSync()) {
      file.delete();
    }
    final png = img.encodePng(imgDecode);

    await file.writeAsBytes(png);
    imageList.add(file);
    time.value?.cancel();
    imageSaved.value = true;
  }

  goNext() {
    imageSaved.value = false;
    start.value = 5;
    currentStep.value++;
  }

  onSubmit() {
    isCompletedAllStep.value = false;
    Get.close(1);
  }

  resetProcess(Function? onPressed) {
    imageList.clear();
    imageSaved.value = false;
    start.value = 5;
    currentStep.value = 1;
    onPressed?.call();
  }

  bool isFaceInCenter(Face face) {
    final double centerX = face.boundingBox.center.dx;
    final double centerY = face.boundingBox.center.dy;

    final double screenCenterX = MediaQuery.of(Get.context!).size.width / 2;
    final double screenCenterY = MediaQuery.of(Get.context!).size.height / 2;

    double tolerance = 250;
    final isFaceCenter = (centerX > screenCenterX - tolerance &&
        centerX < screenCenterX + tolerance &&
        centerY > screenCenterY - tolerance &&
        centerY < screenCenterY + tolerance);
    return isFaceCenter;
  }

  Future<void> _detectCondition({
    required InputImage inputImage,
    required bool stepCondition,
    Function? onSavedImage,
  }) async {
    if (imageSaved.value) return;
    if (stepCondition) {
      isCorrectCondition.value = true;
      _startTimer();
      if (start.value == 0) {
        await _saveImage(
          inputImage: inputImage,
        );
        onSavedImage?.call();
      }
    } else {
      isCorrectCondition.value = false;
      resetTimer();
    }
  }

  bool _stepOne(Face face) {
    return (face.headEulerAngleY ?? 0.0) <= 10 &&
        (face.headEulerAngleY ?? 0.0) >= -10 &&
        isFaceInCenter(face);
  }

  bool _stepTwo(Face face) {
    return (face.headEulerAngleY ?? 0.0) > 10 && isFaceInCenter(face);
  }

  bool _stepThree(Face face) {
    return (face.headEulerAngleY ?? 0.0) < -10 && isFaceInCenter(face);
  }
}
