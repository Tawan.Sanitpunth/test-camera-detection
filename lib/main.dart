import 'dart:io';

import 'package:face_detector_test/pages/face_detector_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/face_detect_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final FaceDetectController stepController = Get.put(FaceDetectController());
  void _goToFaceScan() {
    stepController.resetProcess(
      () => Get.to(
        () => const FaceDetectorView(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Obx(
        () => stepController.imageList.isEmpty
            ? const Center(
                child: Text('Image is empty now'),
              )
            : ListView.builder(
                itemCount: stepController.imageList.length,
                itemBuilder: (context, index) {
                  if (stepController.imageList.isEmpty) {
                    return Container();
                  } else {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.file(
                        File(stepController.imageList[index].path),
                      ),
                    );
                  }
                },
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _goToFaceScan,
        tooltip: 'Go to face scan',
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}
