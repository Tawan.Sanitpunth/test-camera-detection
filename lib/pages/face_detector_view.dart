import 'dart:async';
import 'dart:math';

import 'package:camera/camera.dart';
import 'package:face_detector_test/pages/stepper_with_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';

import '../controller/face_detect_controller.dart';
import 'detector_view.dart';
import 'painter/face_detector_painter.dart';

class FaceDetectorView extends StatefulWidget {
  const FaceDetectorView({super.key});

  @override
  State<FaceDetectorView> createState() => _FaceDetectorViewState();
}

class _FaceDetectorViewState extends State<FaceDetectorView> {
  final FaceDetectController stepController = Get.find();
  List<Widget> listStepWidget = [];
  final FaceDetector _faceDetector = FaceDetector(
    options: FaceDetectorOptions(
      performanceMode: FaceDetectorMode.fast,
      enableContours: true,
      enableLandmarks: true,
      enableTracking: true,
      enableClassification: true,
    ),
  );
  bool _canProcess = true;
  bool _isBusy = false;
  CustomPaint? _customPaint;
  String? _text;
  final randomNum = Random();
  var _cameraLensDirection = CameraLensDirection.front;

  @override
  void initState() {
    listStepWidget = [
      _stepWidget(
        label: 'Please scan your straight face',
      ),
      _stepWidget(
        label: 'Please turn left',
      ),
      _stepWidget(
        label: 'Please turn right',
      ),
    ];
    super.initState();
  }

  @override
  void dispose() {
    _canProcess = false;
    stepController.time.value?.cancel();
    _faceDetector.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DetectorView(
      title: 'Face Detector',
      customPaint: _customPaint,
      text: _text,
      onImage: _processImage,
      initialCameraLensDirection: _cameraLensDirection,
      onCameraLensDirectionChanged: (value) => _cameraLensDirection = value,
    );
  }

  Future<void> _processImage(InputImage inputImage) async {
    if (!_canProcess) return;
    if (_isBusy) return;
    _isBusy = true;
    setState(() {
      _text = '';
    });
    final faces = await _faceDetector.processImage(inputImage);
    if (inputImage.metadata?.size != null &&
        inputImage.metadata?.rotation != null) {
      final painter = FaceDetectorPainter(
        faces,
        inputImage.metadata!.size,
        inputImage.metadata!.rotation,
        _cameraLensDirection,
      );
      _customPaint = CustomPaint(
        painter: painter,
        child: faces.isNotEmpty
            ? StepperWithIndicator(
                stepView: listStepWidget,
                face: faces.first,
                inputImage: inputImage,
              )
            : noFaceDetectWidget(),
      );
    } else {
      String text = 'Faces found: ${faces.length}\n\n';
      for (final face in faces) {
        text += 'face: ${face.boundingBox}\n\n';
      }
      _text = text;
      // TODO: set _customPaint to draw boundingRect on top of image
      _customPaint = null;
    }
    _isBusy = false;
    if (mounted) {
      setState(() {});
    }
  }

  Widget _stepWidget({
    required String label,
    bool isInCorrect = false,
  }) {
    return Obx(
      () => Container(
        padding: const EdgeInsets.symmetric(vertical: 24.0),
        decoration: BoxDecoration(
          color: !stepController.isCorrectCondition.value || isInCorrect
              ? Colors.red.withOpacity(0.2)
              : Colors.green.withOpacity(0.2),
          borderRadius: BorderRadius.circular(12.0),
          border: Border.all(
            color: Colors.white,
          ),
        ),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
              decoration: BoxDecoration(
                  color: Colors.greenAccent,
                  borderRadius: BorderRadius.circular(8.0)),
              child: Text(
                stepController.isCompletedAllStep.value
                    ? 'Congrats you complete face detect process!'
                    : stepController.imageSaved.value
                        ? 'You completed this step.\n Please press next button.'
                        : label,
                textAlign: TextAlign.center,
              ),
            ),
            stepController.time.value != null &&
                    !stepController.imageSaved.value
                ? Text(
                    '${stepController.start.value}',
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w800),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Widget noFaceDetectWidget() {
    stepController.resetTimer();
    return Container(
      decoration: BoxDecoration(
        color: Colors.red.withOpacity(0.2),
      ),
      child: const Column(
        children: [
          Center(
            child: Text('Please scan your face'),
          ),
        ],
      ),
    );
  }
}
