import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../controller/face_detect_controller.dart';

class StepperWithIndicator extends StatefulWidget {
  final List<Widget> stepView;
  final Face face;
  final InputImage inputImage;
  const StepperWithIndicator({
    super.key,
    required this.stepView,
    required this.face,
    required this.inputImage,
  });

  @override
  State<StepperWithIndicator> createState() => _StepperWithIndicatorState();
}

class _StepperWithIndicatorState extends State<StepperWithIndicator> {
  final FaceDetectController stepController = Get.find();

  getWidget(int currentIndex) {
    if (stepController.currentStep.value == currentIndex) {
      return widget.stepView[currentIndex - 1];
    }
  }

  @override
  Widget build(BuildContext context) {
    stepController.stepProcess(
        face: widget.face, inputImage: widget.inputImage);
    return Obx(
      () => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(
                    12.0,
                  ),
                  bottomRight: Radius.circular(
                    12.0,
                  ),
                ),
              ),
              child: getWidget(stepController.currentStep.value),
            ),
          ),
          Container(
            color: Colors.black.withOpacity(0.8),
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Column(
              children: <Widget>[
                Center(
                  child: AnimatedSmoothIndicator(
                    activeIndex: stepController.currentStep.value - 1,
                    count: 3,
                    textDirection: TextDirection.ltr,
                    effect: CustomizableEffect(
                      spacing: 4,
                      activeDotDecoration: const DotDecoration(
                        width: 35.0,
                        height: 11.0,
                        color: Colors.purple,
                        borderRadius: BorderRadius.all(
                          Radius.circular(12.0),
                        ),
                      ),
                      dotDecoration: DotDecoration(
                        width: 10.0,
                        height: 10.0,
                        color: Colors.purple.withOpacity(0.2),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(12.0),
                        ),
                      ),
                      inActiveColorOverride: (index) {
                        if (stepController.currentStep.value > index) {
                          return Colors.purple.withOpacity(0.2);
                        }
                        return Colors.grey.shade200;
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16.0,
                ),
                SizedBox(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      stepController.currentStep.value == 3
                          ? Visibility(
                              visible: stepController.imageSaved.value,
                              child: ElevatedButton(
                                onPressed: () {
                                  stepController.onSubmit();
                                },
                                child: const Text('Submit'),
                              ),
                            )
                          : Visibility(
                              visible: stepController.imageSaved.value,
                              child: ElevatedButton(
                                onPressed: () {
                                  stepController.goNext();
                                },
                                child: const Text('Next'),
                              ),
                            ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
